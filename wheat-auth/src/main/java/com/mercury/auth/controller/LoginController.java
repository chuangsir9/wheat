package com.mercury.auth.controller;

import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

/**
 * @author by mercury
 * @Date 2022/2/8 15:03
 */
@Slf4j
@Api(tags = "登录接口")
@RestController
public class LoginController {


    @GetMapping("/login")
    public ModelAndView require() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("login");
        return modelAndView;
    }

}
