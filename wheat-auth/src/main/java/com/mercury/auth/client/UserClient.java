package com.mercury.auth.client;

import com.mercury.auth.client.model.UserDto;
import org.springframework.stereotype.Component;

/**
 * @author by mercury
 * @Date 2022/2/8 15:05
 */
@Component
public interface UserClient {

    UserDto findByUsername(String username);

}
