package com.mercury.auth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;

/**
 * @author by mercury
 * @Date 2022/2/8 14:45
 */
@SpringBootApplication
@EnableResourceServer
@ComponentScan({"com.mercury.auth","com.mercury.security.config"})
public class Auth {
    public static void main(String[] args) {
        SpringApplication.run(Auth.class);
    }
}
