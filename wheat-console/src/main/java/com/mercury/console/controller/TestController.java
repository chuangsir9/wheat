package com.mercury.console.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author by mercury
 * @Date 2022/2/8 13:50
 */
@Slf4j
@Api(tags = "测试接口")
@RequestMapping(value = "/${api.web.version:v1}/${api.web.path:/api/test}")
@RestController
public class TestController {


    @ApiOperation("测试接口")
    @GetMapping("/test")
    public void show(){

        System.out.println("test");
    }

}
