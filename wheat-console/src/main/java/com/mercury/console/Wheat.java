package com.mercury.console;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author by zhangchuang
 * @Date 2022/2/8 13:32
 */

@SpringBootApplication
public class Wheat {
    public static void main(String[] args) {
        SpringApplication.run(Wheat.class);
    }
}
